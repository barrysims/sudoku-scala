import org.scalatest.{Matchers, FlatSpec}

class SudokuTest extends FlatSpec with Matchers {

  val puzzle =
   ("300040005" +
    "020008493" +
    "000091000" +
    "003000500" +
    "007805200" +
    "001000900" +
    "000210000" +
    "512900060" +
    "800070004").toList.map(_ - '0')

  val solution = Some(
   ("398642715" +
    "126758493" +
    "745391682" +
    "483129576" +
    "967835241" +
    "251467938" +
    "674213859" +
    "512984367" +
    "839576124").toList.map(_ - '0'))

  "solve" should "return a solved puzzle" in {
    Sudoku.solve(puzzle) should equal(solution)
  }
}

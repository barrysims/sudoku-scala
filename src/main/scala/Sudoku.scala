
object Sudoku {

  /**
   * Solver for sudoku puzzles
   *
   * @param a Array[Int] puzzle
   * @return the solution, or null, if no solution exists
   */

  def solve(a: List[Int]): Option[List[Int]] = {

    a.indexOf(0) match {
      case -1 => Some(a) //success
      case n =>
        available(n, a) match {
          case List() => None //fail
          case remain =>
            object FindSolution { def unapply(i: Int): Option[List[Int]] = solve(a.updated(n, i)) }
            remain.collectFirst { case FindSolution(s) => s} //keep searching
        }
    }
  }

  def available(i: Int, a: List[Int]) = {

    val a9 = a.grouped(9).toList
    val rowNums = a9(i / 9)
    val colNums = (0 to 8).map(a9(_)(i % 9))

    val s = i / 27 * 9 + i % 9 / 3
    val a3 = a.grouped(3).toList
    val squNums = a3(s) ++ a3(s + 3) ++ a3(s + 6)

    (1 to 9).filterNot(n => (squNums ++ rowNums ++ colNums).distinct.contains(n))
  }
}